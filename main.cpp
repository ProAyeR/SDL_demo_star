#include <iostream>
#include <cmath>
#include <windows.h>
#include <string>
#include <vector>
#include <random>
#include <chrono>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

/*****************最小随机数模块******************/
// #include <time.h>
// #define randomInit() srand((unsigned)time(NULL))
// #define randomInt(a, b) (rand() % (b - a + 1) + a)
/************************************************/

// 限制函数
template <typename type>
inline type limit_in(const type x, const type min, const type max) { return x <= min ? min : (x >= max ? max : x); }
template <typename type>
inline type more_than(const type x, const type min) { return x <= min ? min : x; }
template <typename type>
inline type less_than(const type x, const type max) { return x >= max ? max : x; }
// 进位函数
template <typename type>
inline type carry_in(const type x, const type min, const type max) { return x < min ? x + max - min : (x > max ? x - max + min : x); }

//数学常数
#define Pi 3.1415926
// #define Pi 3.141592653589793 // 不需要如此高的精度

using namespace std;

char *nowtime()
{
    static char CurrentTime[16];
    auto now = chrono::system_clock::now();
    //通过不同精度获取相差的毫秒数
    uint64_t dis_millseconds = chrono::duration_cast<chrono::milliseconds>(now.time_since_epoch()).count() -
                               chrono::duration_cast<chrono::seconds>(now.time_since_epoch()).count() * 1000;
    time_t tt = chrono::system_clock::to_time_t(now);
    auto time_tm = localtime(&tt);
    int hours = time_tm->tm_hour;
    int minutes = time_tm->tm_min;
    int seconds = time_tm->tm_sec;
    int milliseconds = (int)dis_millseconds;
    sprintf(CurrentTime, "%02d:%02d:%02d.%03d", hours, minutes, seconds, milliseconds);
    return CurrentTime;
}

void dbg_log(string str)
{
    printf("[%s]debug: %s\n", nowtime(), str.c_str());
    //任何控制台的输出会大幅影响帧率，请小心使用
}
void dbg_logError()
{
    printf("[%s]debug: SDL_GetError()如下: %s\n", nowtime(), SDL_GetError());
}
void dbg_pause(int pos) //用编译宏__LINE__来调用debug_pause函数
{
    printf("[%s]debug: 当前控制台已触发暂停,暂停位置:%d\n", nowtime(), pos);
    dbg_logError();
    system("pause>nul");
}

inline bool operator==(SDL_Point &a, SDL_Point &b) { return (a.x == b.x && a.y == b.y); }
inline SDL_Point operator*(SDL_Point a, double b) { return {int(double(a.x) * b), int(double(a.y) * b)}; }
inline SDL_Rect operator*(SDL_Rect a, double b) { return {int(double(a.x) * b), int(double(a.y) * b), int(double(a.w) * b), int(double(a.h) * b)}; }
inline SDL_Point operator+(SDL_Point a, SDL_Point b) { return {a.x + b.x, a.y + b.y}; }
inline SDL_Rect operator+(SDL_Rect a, SDL_Point b) { return {a.x + b.x, a.y + b.y, a.w, a.h}; }
inline double distance(SDL_Point a, SDL_Point b) { return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2)); }
inline bool is_in_rect_positive(SDL_Rect a, SDL_Point b) { return ((b.x >= a.x && b.y >= a.y && b.x <= a.x + a.w && b.y <= a.y + a.h)); }
inline SDL_Rect fix_negative_rect(SDL_Rect a) { return (a.w < 0) ? a.x += a.w, a.w *= -1, a : (a.h < 0) ? a.y += a.h, a.h *= -1, a : a; }
inline bool is_in_rect(SDL_Rect a, SDL_Point b) { return is_in_rect_positive(fix_negative_rect(fix_negative_rect(a)), b); }

class Star
{
private:
    int index;
    Star(int i, SDL_Point pp, SDL_Texture *pss) : index(i), pos(pp), pos_before(pp), texture(pss){}; //不允许自行添加Star类

public:
    SDL_Point pos, pos_before;
    SDL_Texture *texture;
    bool is_selected = false;
    friend class StarManager;
};

class StarManager
{
public:
    vector<Star> datas;
    vector<vector<bool>> connections;
    int stars_max_connections; //规定星星最多能够连接的数量

    StarManager() : stars_max_connections(5) {}

    bool new_star(SDL_Point pos, SDL_Texture *texture)
    {
        for (int i = 0; i < datas.size(); i++)
            if (pos == datas[i].pos)
                return false;
        datas.push_back(Star(datas.size(), pos, texture));
        vector<bool> tempNewStar;
        for (int i = 0; i < connections.size(); i++)
        {
            connections[i].push_back(false);
            tempNewStar.push_back(false);
        }
        tempNewStar.push_back(false); //多添加一个角
        connections.push_back(tempNewStar);
        // if (connections.size() == connections.at(0).size())
        //     return true;
        return true;
    }

    void add_connection(Star *s1, Star *s2)
    {
        if (s1 == s2)
            return;
        connections[s1->index][s2->index] = true;
        connections[s2->index][s1->index] = true;
    }

    int SelectStars(SDL_Rect rect)
    {
        int return_val = 0;
        for (int i = 0; i < datas.size(); i++)
            if (datas[i].is_selected = is_in_rect(rect, datas[i].pos))
            {
                datas[i].pos_before = datas[i].pos; //选中时认定之前位置为当前位置
                return_val++;
            }
        return return_val;
    }

    vector<int> SelectedStars()
    {
        vector<int> return_temp;
        return_temp.clear();
        for (int i = 0; i < datas.size(); i++)
            if (datas[i].is_selected)
                return_temp.push_back(i);
        return return_temp;
    }

    bool DragStar(Star *star, SDL_Point hold_point, SDL_Point now_point)
    {
        SDL_Rect tempRect = {star->pos_before.x - 16, star->pos_before.y - 16, 32, 32};
        if (is_in_rect(tempRect, hold_point))
        {
            star->pos = now_point;
        }
        else
            return false;
        return true;
    }

#define RANDOM_SPAWN -1
#define DEVICE_RANDOM_SPAWN 0
    void RandomSpawnStars(vector<SDL_Texture *> texture_list, long seed, SDL_Rect rect, int stars_number)
    {
        //设置随机数方式
        if (seed == DEVICE_RANDOM_SPAWN)
        {
            random_device random_device_seed; //硬件生成随机数种子
            seed = random_device_seed();
        }
        else if (seed == RANDOM_SPAWN)
            seed = (unsigned long)time(nullptr); //否则就直接使用提供的seed
        ranlux48 engine(seed);                   //利用种子生成随机数引擎

        uniform_int_distribution<int> distrib_x(rect.x, rect.x + rect.w); //设置随机数范围，并为均匀分布
        uniform_int_distribution<int> distrib_y(rect.y, rect.y + rect.h);
        uniform_int_distribution<int> distrib_texture_index(0, texture_list.size() - 1); //别忘了减一
        //尝试生成星星
        for (int i = 0; datas.size() <= stars_number; i++)
        {
            SDL_Point tempPos;
            int texture_index = distrib_texture_index(engine); //随机纹理序号
            tempPos.x = distrib_x(engine);                     //随机位置
            tempPos.y = distrib_y(engine);
            new_star(tempPos, texture_list.at(texture_index));
        }

        //为星星添加连接
        for (int i = 0; i < datas.size(); i++)
        {
            //循环选择连接的星系
            double *distant = new double[datas.size()];
            Star **temp_dis = new Star *[datas.size()];

            for (int j = 0; j < datas.size(); j++) //计算每个星距离当前星的距离
            {
                distant[j] = distance(datas[i].pos, datas[j].pos);
                temp_dis[j] = &datas[j];
            }

            for (int j = 0; j < datas.size(); j++) //排序算法
                for (int k = 0; k < j; k++)
                    if (distant[j] < distant[k])
                    {
                        swap(distant[j], distant[k]); //按照距离进行升序排序，交换时把指针数组也进行交换
                        swap(temp_dis[j], temp_dis[k]);
                    }

            for (int j = 0; j < stars_max_connections; j++) //最后取有限个数进行连接就好
                add_connection(&datas[i], temp_dis[j]);
        }
    }

    void RenderStars(SDL_Renderer *renderer)
    {
        //渲染星星连接线
        SDL_SetRenderDrawColor(renderer, 156, 220, 254, 255); // blue
        for (int i = 0; i < datas.size(); i++)
            for (int j = 0; j < i; j++)
                if (connections[i][j])
                {
                    SDL_RenderDrawLine(renderer, datas[i].pos.x, datas[i].pos.y, datas[j].pos.x, datas[j].pos.y);
                }

        //渲染每个星星材质
        for (int i = 0; i < datas.size(); i++)
        {
            SDL_Rect tempRect = {datas[i].pos.x - 16, datas[i].pos.y - 16, 32, 32};
            SDL_RenderCopy(renderer, datas[i].texture, NULL, &tempRect);
            if (datas[i].is_selected)
            {
                SDL_SetRenderDrawColor(renderer, 0, 255, 0, 128);
                SDL_RenderDrawRect(renderer, &tempRect);
            }
        }
    }
};

class DotShip
{
private:
    int index;
    DotShip(int i, SDL_Color c, SDL_Point p) : index(i), color(c), pos(p) {} //不允许自行添加DotShip类

public:
    SDL_Color color;
    SDL_Point pos, pos_before;
    double direction_angle = 1.0;
    double speed = 0.0;
    bool is_selected = false;
    bool is_targeted = false;
    SDL_Point target_pos;
    friend class DotShipManager;
};

class DotShipManager
{
private:
    SDL_Point border;

public:
    vector<DotShip> datas;
    DotShipManager(int w, int h) : border({w, h}) {}

    bool new_ship(SDL_Color c, SDL_Point p)
    {
        datas.push_back(DotShip(datas.size(), c, p));
        datas.back().pos_before = p;
        return true;
    }

    void RandomSpawnShips(long seed, int ships_number)
    {
        //设置随机数方式
        if (seed == DEVICE_RANDOM_SPAWN)
        {
            random_device random_device_seed; //硬件生成随机数种子
            seed = random_device_seed();
        }
        else if (seed == RANDOM_SPAWN)
            seed = (unsigned long)time(nullptr); //否则就直接使用提供的seed
        ranlux48 engine(seed);                   //利用种子生成随机数引擎

        uniform_int_distribution<int> distrib_x(0, border.x); //设置随机数范围，并为均匀分布
        uniform_int_distribution<int> distrib_y(0, border.y);
        uniform_int_distribution<int> distrib_clr(0, 255);
        uniform_int_distribution<int> distrib_angle(0, 359);
        uniform_int_distribution<int> distrib_speed(3, 10);

        //尝试生成点船
        for (int i = 0; datas.size() <= ships_number; i++)
        {
            SDL_Point tempPos;
            tempPos.x = distrib_x(engine); //随机位置
            tempPos.y = distrib_y(engine);
            SDL_Color tempColor = {0, 0, 0, 255};
            while (tempColor.r + tempColor.g + tempColor.b < 128 * 3)
            {
                tempColor.r = distrib_clr(engine);
                tempColor.g = distrib_clr(engine);
                tempColor.b = distrib_clr(engine);
            }
            dbg_log("random color RGB:" + to_string(tempColor.r) + ", " + to_string(tempColor.g) + ", " + to_string(tempColor.b));
            new_ship(tempColor, tempPos);
            datas.back().direction_angle = (double)distrib_angle(engine);
            datas.back().speed = (double)distrib_speed(engine);
        }
        dbg_log(" note: 输出随机的颜色已确保性能不会被影响");
    }

    void ship_move(DotShip &ship)
    {
        double direction_vector[2] = {0, 0};
        direction_vector[0] = cos(ship.direction_angle * Pi / 180.0);
        direction_vector[1] = sin(ship.direction_angle * Pi / 180.0);
        ship.pos_before = ship.pos;
        ship.pos.x += direction_vector[0] * ship.speed;
        ship.pos.x = limit_in(ship.pos.x, 0, border.x - 1); //减一确保可见
        ship.pos.y += direction_vector[1] * ship.speed;
        ship.pos.y = limit_in(ship.pos.y, 0, border.y - 1);
    }

    void ship_tick()
    {
        ranlux48 engine((unsigned long)time(nullptr));
        uniform_int_distribution<int> distrib_delta(-5, 5);
        for (int i = 0; i < datas.size(); i++)
        {
            if (datas[i].is_targeted)
            {
                if (distance(datas[i].pos, datas[i].target_pos) < 6.0)
                    datas[i].is_targeted = false;
                else
                {
                    double target_angle;
                    if (abs(datas[i].target_pos.x - datas[i].pos.x) < 0.01)
                        if (datas[i].target_pos.y - datas[i].pos.y > 0)
                            target_angle = 90.0;
                        else
                            target_angle = 270.0;
                    else
                    {
                        double x_delta, y_delta;
                        x_delta = datas[i].target_pos.x - datas[i].pos.x;
                        y_delta = datas[i].target_pos.y - datas[i].pos.y;
                        // 第二次优化寻路角度算法
                        if (y_delta <= x_delta && y_delta > 0) // 1号区域
                            target_angle = 0.0 + atan(y_delta / x_delta) * 180.0 / Pi;
                        else if (y_delta > x_delta && x_delta >= 0) // 2号区域
                            target_angle = 90.0 - (atan(x_delta / y_delta) * 180.0 / Pi);
                        else if (y_delta >= -x_delta && x_delta < 0) // 3号区域
                            target_angle = 90.0 + (atan(-x_delta / y_delta) * 180.0 / Pi);
                        else if (y_delta < -x_delta && y_delta >= 0) // 4号区域
                            target_angle = 180.0 - (atan(y_delta / -x_delta) * 180.0 / Pi);
                        else if (y_delta >= x_delta && y_delta < 0) // 5号区域
                            target_angle = 180.0 + (atan(-y_delta / -x_delta) * 180.0 / Pi);
                        else if (y_delta < x_delta && x_delta <= 0) // 6号区域
                            target_angle = 270.0 - (atan(-x_delta / -y_delta) * 180.0 / Pi);
                        else if (y_delta <= -x_delta && x_delta > 0) // 7号区域
                            target_angle = 270.0 + (atan(x_delta / -y_delta) * 180.0 / Pi);
                        else if (y_delta > -x_delta && y_delta <= 0) // 8号区域
                            target_angle = 0.0 - atan(-y_delta / x_delta) * 180.0 / Pi;
                        //别忘了atan值域只有-90~+90，且我只使用0~+45之间
                    }
                    target_angle = carry_in(target_angle, 0.0, 360.0);
                    // 优化转向算法
                    if (abs(datas[i].direction_angle - target_angle) < 7.0)
                        datas[i].direction_angle = target_angle;
                    else if (datas[i].direction_angle - target_angle < 180.0 && datas[i].direction_angle - target_angle > 0.0 || //注意&&的运算机比||高
                             datas[i].direction_angle - target_angle < -180.0)
                        datas[i].direction_angle -= 5.0;
                    else
                        datas[i].direction_angle += 5.0;
                    // datas[i].direction_angle = target_angle;
                    datas[i].direction_angle = carry_in(datas[i].direction_angle, 0.0, 360.0);
                    ship_move(datas[i]);
                }
            }
            else
            {
                ship_move(datas[i]);
                // datas[i].speed += distrib_delta(engine);
                datas[i].direction_angle = datas[i].direction_angle + distrib_delta(engine);
                datas[i].direction_angle = carry_in(datas[i].direction_angle, 0.0, 360.0);
            }
        }
    }

    int SelectShips(SDL_Rect rect)
    {
        int return_val = 0;
        for (int i = 0; i < datas.size(); i++)
            if (datas[i].is_selected = is_in_rect(rect, datas[i].pos))
                return_val++;
        return return_val;
    }

    vector<int> SelectedShips()
    {
        vector<int> return_temp;
        return_temp.clear();
        for (int i = 0; i < datas.size(); i++)
            if (datas[i].is_selected)
                return_temp.push_back(i);
        return return_temp;
    }

    void RenderShips(SDL_Renderer *renderer)
    {
        for (int i = 0; i < datas.size(); i++)
        {
            SDL_SetRenderDrawColor(renderer, datas[i].color.r, datas[i].color.g, datas[i].color.b, datas[i].color.a);
            SDL_RenderDrawLine(renderer, datas[i].pos.x, datas[i].pos.y, datas[i].pos_before.x, datas[i].pos_before.y);
            if (datas[i].is_selected)
            {
                SDL_Rect tempRect = {datas[i].pos.x - 5, datas[i].pos.y - 5, 10, 10};
                if (datas[i].is_targeted)
                    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 128);
                else
                    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 128);
                SDL_RenderDrawRect(renderer, &tempRect);
                if (datas[i].is_targeted)
                    SDL_RenderDrawLine(renderer, datas[i].pos.x, datas[i].pos.y, datas[i].target_pos.x, datas[i].target_pos.y);
            }
        }
    }
};

void RenderText(SDL_Renderer *renderer, string str, TTF_Font *font, SDL_Rect *target)
{
    SDL_Surface *pTextSurface = NULL;
    SDL_Texture *pTextTexture = NULL;
    pTextSurface = TTF_RenderUTF8_Blended(font, str.c_str(), {255, 255, 255});
    pTextTexture = SDL_CreateTextureFromSurface(renderer, pTextSurface);
    target->w = pTextSurface->w;
    target->h = pTextSurface->h; //会修改到输入的矩阵中
    SDL_RenderCopy(renderer, pTextTexture, NULL, target);
}

bool IsQuit()
{
    int ID = 0;
    ID = MessageBoxW(NULL, L"你确定要退出吗", L"退出", MB_ICONQUESTION | MB_YESNO);
    return (ID == IDYES);
}

// how to build - use "makeSDL main.cpp -n"
int main(int argc, char *argv[])
{
    //基础初始化
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleOutputCP(65001); // chcp 65001
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
    TTF_Init();

    dbg_log("初始化完毕");

    int window_w = 1480, window_h = 720;
    SDL_Window *window = SDL_CreateWindow("SDL_stars", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          window_w, window_h, SDL_WINDOW_SHOWN);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC); //垂直同步

    SDL_Rect screen_rect = {0};                                //表示窗口的大小
    SDL_GetWindowSize(window, &screen_rect.w, &screen_rect.h); //获得窗口大小矩形
    dbg_log("窗口大小:" + to_string(screen_rect.w) + ", " + to_string(screen_rect.h));

    //材质初始化
    const int res_star_png_count = 17;
    const char *res_star_png_list[res_star_png_count] = {
        ".\\res\\stars\\1_a_star.png",
        ".\\res\\stars\\2_b_star.png",
        ".\\res\\stars\\3_f_star.png",
        ".\\res\\stars\\4_g_star.png",
        ".\\res\\stars\\5_k_star.png",
        ".\\res\\stars\\6_m_star.png",
        ".\\res\\stars\\7_sc_m_giant.png",
        ".\\res\\stars\\8_t_star.png",
        ".\\res\\stars\\9_black_hole.png",
        ".\\res\\stars\\10_neutron_star.png",
        ".\\res\\stars\\11_pulsar.png",
        ".\\res\\stars\\12_a_binary_star.png",
        ".\\res\\stars\\13_b_binary_star.png",
        ".\\res\\stars\\14_c_binary_star.png",
        ".\\res\\stars\\15_d_binary_star.png",
        ".\\res\\stars\\16_e_binary_star.png",
        ".\\res\\stars\\17_a_trinary_star.png",
    };
    vector<SDL_Surface *> Star_Surface_List;
    vector<SDL_Texture *> Star_Texture_List;

    const int res_space_sky_png_count = 7;
    const char *res_space_sky_png_list[res_star_png_count] = {
        ".\\res\\bg\\space_sky_0.png",
        ".\\res\\bg\\space_sky_1.png",
        ".\\res\\bg\\space_sky_2.png",
        ".\\res\\bg\\space_sky_3.png",
        ".\\res\\bg\\space_sky_4.png",
        ".\\res\\bg\\space_sky_5.png",
        ".\\res\\bg\\space_sky_6.png",
    };
    vector<SDL_Surface *> Space_Sky_Surface_List;
    vector<SDL_Texture *> Space_Sky_Texture_List;

    // 加载星星的png图片
    for (int i = 0; i < res_star_png_count; i++)
    {
        dbg_log("加载材质:" + string(res_star_png_list[i]));
        Star_Surface_List.push_back(IMG_Load(res_star_png_list[i]));
        Star_Texture_List.push_back(SDL_CreateTextureFromSurface(renderer, Star_Surface_List.back()));
    }

    // 加载星空的png图片
    for (int i = 0; i < res_space_sky_png_count; i++)
    {
        dbg_log("加载材质:" + string(res_space_sky_png_list[i]));
        Space_Sky_Surface_List.push_back(IMG_Load(res_space_sky_png_list[i]));
        Space_Sky_Texture_List.push_back(SDL_CreateTextureFromSurface(renderer, Space_Sky_Surface_List.back()));
    }

    //字体初始化
    TTF_Font *font;
    font = TTF_OpenFont(".\\res\\fonts\\QuanPixel.ttf", 12);
    dbg_log("加载字体:" + (string) ".\\res\\fonts\\QuanPixel.ttf");

    //地图数据相关
    const SDL_Rect rMap = {0, 0, static_cast<int>(window_w * 2), static_cast<int>(window_h * 2)};

    //星图数据初始化
    SDL_Rect rMapTextureRect = rMap; // x&y :地图渲染位置, w&h :渲染大小
    SDL_Rect rMapTextureRect_hold = rMapTextureRect;
    StarManager star_manager;
    { //生成星星位置限制
        SDL_Rect tempStarsSpawnRect = rMap;
        tempStarsSpawnRect.x = rMap.w * 0.05;
        tempStarsSpawnRect.y = rMap.h * 0.05;
        tempStarsSpawnRect.w *= 0.9;
        tempStarsSpawnRect.h *= 0.9;
        star_manager.RandomSpawnStars(Star_Texture_List, RANDOM_SPAWN, tempStarsSpawnRect, window_h / 10);
    }
    dbg_log("star_manager.RandomSpawnStars 完成, 星星数量: " + to_string(star_manager.datas.size()));

    //飞船数据初始化
    DotShipManager ship_manager(rMap.w, rMap.h);
    ship_manager.RandomSpawnShips(RANDOM_SPAWN, 100);
    dbg_log("ship_manager.RandomSpawnShips 完成, 点船数量: " + to_string(ship_manager.datas.size()));

    //循环前初始化
    bool quit = false;

    //用于接受SDL媒体输入事件
    SDL_Event event;

    //用于处理鼠标移动事件
    SDL_Point mouse_point_current = {0};
    SDL_Point mouse_point_before = {0};

    //用于处理鼠标点击事件
#define MOUSE_BUTTON_LEFT 1   // SDL_BUTTON_LEFT
#define MOUSE_BUTTON_MIDDLE 2 // SDL_BUTTON_MIDDLE
#define MOUSE_BUTTON_RIGHT 3  // SDL_BUTTON_RIGHT
#define MOUSE_BUTTON_RELEASE_LEFT -1
#define MOUSE_BUTTON_RELEASE_MIDDLE -2
#define MOUSE_BUTTON_RELEASE_RIGHT -3
    int mouse_hold = 0;
    unsigned int mouse_time_stamp = 0, mouse_continuous_time = 0;
    bool mouse_motion = false;
#define MOUSE_STATE_NONE 0
#define MOUSE_STATE_DRAG 1
#define MOUSE_STATE_CLICK 2
#define MOUSE_STATE_DRAG_RELEASE 3
#define MOUSE_STATE_CLICK_RELEASE 4
    int mouse_state = MOUSE_STATE_NONE;
    SDL_Point mouse_point_hold = {0};
    SDL_Rect mouse_rect_select = {0};

    //用于处理鼠标滑轮事件
    int mouse_wheel = 0;
    int zoomlevel = 10;
#define zoomlevel_min 5
#define zoomlevel_max 20
#define zoomindex (pow(1.1, (zoomlevel - 10)))

    //用于处理地图选择事件
    SDL_Point map_mouse_point_current = {0};
    SDL_Point map_mouse_point_before = {0};
    SDL_Rect rMapSelect = {0};

    // debug文本初始位置
    SDL_Rect rDebugTextRect = {1, 1, 0, 0};
    bool is_info_ship = false;
    DotShip *info_ship = NULL;

    // 平铺背景矩形
    SDL_Rect rSpaceSkyBG = {0, 0, Space_Sky_Surface_List[1]->w, Space_Sky_Surface_List[1]->h};
    SDL_Rect rMapSpaceSkyBG = {0}; //用于实现假透明效果

    //使用地图大小创建材质
    SDL_Texture *star_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                                  rMapTextureRect.w, rMapTextureRect.h);
    dbg_log("地图材质创建完毕, w:" + to_string(rMapTextureRect.w) + ", h:" + to_string(rMapTextureRect.h));

    //用于处理游戏刻
    unsigned long game_tick, game_tick_before, current_tick_count;
    game_tick = game_tick_before = GetTickCount();
    current_tick_count = game_tick - game_tick_before; //用于计算每秒的Ticks
    int tps;

    //用于处理显示当前帧率
    Uint64 fps_tick_start, fps_tick_end;
    int fps;
    float elapsed; //用于帧计算

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////主循环/////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    while (!quit)
    {
        //帧开始
        fps_tick_start = SDL_GetPerformanceCounter();

        //清空上一帧内容
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // 清空为黑色背景
        SDL_RenderClear(renderer);

        //平铺星空(纹理还会再平铺一次)
        for (rSpaceSkyBG.x = 0; rSpaceSkyBG.x < screen_rect.w; rSpaceSkyBG.x += rSpaceSkyBG.w) // TODO: 以后添加设置来开关星空的设置
            for (rSpaceSkyBG.y = 0; rSpaceSkyBG.y < screen_rect.h; rSpaceSkyBG.y += rSpaceSkyBG.h)
                SDL_RenderCopy(renderer, Space_Sky_Texture_List[1], NULL, &rSpaceSkyBG);

        //检测输入事件
        ////这些变量无法检测初始状态
        mouse_wheel = 0;
        mouse_motion = false;
        mouse_hold = more_than(mouse_hold, 0);
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = IsQuit();
                break;
            case SDL_MOUSEMOTION:
                mouse_motion = true;
                mouse_point_current.x = event.motion.x;
                mouse_point_current.y = event.motion.y;
                break;
            case SDL_MOUSEBUTTONDOWN:
                mouse_hold = event.button.button;
                mouse_point_hold.x = event.button.x;
                mouse_point_hold.y = event.button.y;
                mouse_time_stamp = event.button.timestamp;
                break;
            case SDL_MOUSEBUTTONUP:
                mouse_hold = -event.button.button;
                mouse_time_stamp = 0;
                break;
            case SDL_MOUSEWHEEL:
                mouse_wheel = event.wheel.preciseY;
                // mouse_point_current = mouse_point_before;
                break;
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE) //遇到退出键就退出程序
                    quit = IsQuit();
                break;
            }
        }
        ////设置鼠标状态
        if (mouse_hold > 0) // mouse_hold为负时是释放时的键值
            if ((mouse_motion || mouse_state == MOUSE_STATE_DRAG) && abs(mouse_rect_select.w * mouse_rect_select.h) > 50)
                //手感优化，如果矩形太小就视为用户无心拖动，作为点击识别，释放点击
                mouse_state = MOUSE_STATE_DRAG;
            else
                mouse_state = MOUSE_STATE_CLICK;
        else if (mouse_state == MOUSE_STATE_DRAG)
            mouse_state = MOUSE_STATE_DRAG_RELEASE;
        else if (mouse_state == MOUSE_STATE_CLICK)
            mouse_state = MOUSE_STATE_CLICK_RELEASE;
        else
            mouse_state = MOUSE_STATE_NONE;
        ////计算点击持续时间
        mouse_continuous_time = (mouse_hold != 0) ? SDL_GetTicks() - mouse_time_stamp : 0;

        //处理缩放地图事件
        if (mouse_wheel != 0)
        {
            zoomlevel = limit_in(zoomlevel + mouse_wheel, zoomlevel_min, zoomlevel_max);
            rMapTextureRect.w = rMap.w * zoomindex;
            rMapTextureRect.h = rMap.h * zoomindex;
            // rMapTextureRect.x = zoomindex * rMapTextureRect.x + (mouse_point_current.x - map_mouse_point_current.x) * (1 - zoomindex); //写不出来直接开摆
            // rMapTextureRect.y = zoomindex * rMapTextureRect.y + (mouse_point_current.y - map_mouse_point_current.y) * (1 - zoomindex);
        }

        //处理地图移动事件
        ////镜头移动
        if (mouse_hold == MOUSE_BUTTON_RIGHT && mouse_state == MOUSE_STATE_DRAG)
        {
            //镜头视角限制算法 - x&y 限制在一个指定值中
            rMapTextureRect.x = limit_in(rMapTextureRect_hold.x + mouse_point_current.x - mouse_point_hold.x,
                                         screen_rect.w / 2 - rMapTextureRect_hold.w, screen_rect.w / 2);
            rMapTextureRect.y = limit_in(rMapTextureRect_hold.y + mouse_point_current.y - mouse_point_hold.y,
                                         screen_rect.h / 2 - rMapTextureRect_hold.h, screen_rect.h / 2);
            // SDL_RenderDrawLine(renderer, mouse_point_hold.x, mouse_point_hold.y, mouse_point_current.x, mouse_point_current.y);
        }
        else
            rMapTextureRect_hold = rMapTextureRect;

        /////////////////////渲染星星/////////////////
        SDL_SetRenderTarget(renderer, star_texture);               //开始渲染星空材质
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND); //支持透明色绘制
        ////此时坐标系:rMap
        map_mouse_point_current = operator*(operator+(mouse_point_current, {-rMapTextureRect.x, -rMapTextureRect.y}), 1.0 / zoomindex);
        map_mouse_point_current.x = limit_in(map_mouse_point_current.x, 0, rMap.w);
        map_mouse_point_current.y = limit_in(map_mouse_point_current.y, 0, rMap.h);
        SDL_RenderClear(renderer); //清空材质
        // SDL_SetRenderDrawColor(renderer, 8, 8, 32, 1); //星空背景色
        // SDL_RenderFillRect(renderer, &rMap);
        // 平铺星空(假透明效果)
        for (rSpaceSkyBG.x = 0; rSpaceSkyBG.x < screen_rect.w; rSpaceSkyBG.x += rSpaceSkyBG.w)
            for (rSpaceSkyBG.y = 0; rSpaceSkyBG.y < screen_rect.h; rSpaceSkyBG.y += rSpaceSkyBG.h)
            {
                rMapSpaceSkyBG = operator*(operator+(rSpaceSkyBG, {-rMapTextureRect.x, -rMapTextureRect.y}), 1.0 / zoomindex);
                SDL_RenderCopy(renderer, Space_Sky_Texture_List[1], NULL, &rMapSpaceSkyBG);
            }
        ////游戏刻处理
        game_tick = GetTickCount();
        if (game_tick - game_tick_before >= 50)
        {
            ship_manager.ship_tick();
            current_tick_count = game_tick - game_tick_before;
            game_tick_before += 50; //单位，毫秒
        }
        ////根据摄像机位置渲染星星材质
        star_manager.RenderStars(renderer);
        ////渲染点船位置
        ship_manager.RenderShips(renderer);

        ////鼠标选择框
        mouse_rect_select = {mouse_point_hold.x, mouse_point_hold.y,
                             mouse_point_current.x - mouse_point_hold.x, mouse_point_current.y - mouse_point_hold.y};
        ////渲染时鼠标动作检测
        if (mouse_hold == MOUSE_BUTTON_LEFT && mouse_state == MOUSE_STATE_DRAG)
        {
            rMapSelect = operator*(operator+(mouse_rect_select, {-rMapTextureRect.x, -rMapTextureRect.y}), 1.0 / zoomindex);
            if (star_manager.SelectedStars().size() == 1) //拖动星星事件
            {
                if (star_manager.DragStar(&star_manager.datas[star_manager.SelectedStars()[0]],
                                          {rMapSelect.x, rMapSelect.y}, {rMapSelect.x + rMapSelect.w, rMapSelect.y + rMapSelect.h}))
                {
                    SDL_SetRenderDrawColor(renderer, 140, 218, 0, 255); //画出拖动线
                    SDL_RenderDrawLine(renderer, rMapSelect.x, rMapSelect.y, rMapSelect.x + rMapSelect.w, rMapSelect.y + rMapSelect.h);
                }
            }
            else
            { // 否则就正常画出矩形选择框
                SDL_SetRenderDrawColor(renderer, 41, 226, 229, 255);
                SDL_RenderDrawRect(renderer, &rMapSelect);
            }
        }
        if (mouse_hold == MOUSE_BUTTON_RELEASE_LEFT && mouse_state == MOUSE_STATE_DRAG_RELEASE)
        {
            int number = ship_manager.SelectShips(rMapSelect);
            if (is_info_ship = (number == 1))
                info_ship = &ship_manager.datas[0];
            if (number == 0)
                number = star_manager.SelectStars(rMapSelect);
            if (number != 1)
                star_manager.SelectStars({0}); //取消选择
        }
        if (mouse_hold == MOUSE_BUTTON_RELEASE_LEFT && mouse_state == MOUSE_STATE_CLICK_RELEASE)
        {
            vector<int> temp;
            temp = ship_manager.SelectedShips();
            for (int i = 0; i < temp.size(); i++)
            {
                ship_manager.datas[temp[i]].is_targeted = true;
                ship_manager.datas[temp[i]].target_pos = map_mouse_point_current;
            }
        }
        SDL_SetRenderTarget(renderer, NULL); //渲染目标设置回窗口
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

        //地图事件处理完毕，显示地图
        ////修正缩放后地图的位置变化
        if (mouse_wheel != 0)
        {
            rMapTextureRect.x -= (map_mouse_point_before.x - map_mouse_point_current.x) * zoomindex - 1;
            rMapTextureRect.y -= (map_mouse_point_before.y - map_mouse_point_current.y) * zoomindex - 1;
        }
        else
            map_mouse_point_before = map_mouse_point_current; //放在这个位置才不会莫名跳跃(其他位置就会莫名跳跃，算是bug)
        SDL_RenderCopy(renderer, star_texture, NULL, &rMapTextureRect);

        //渲染鼠标特效
        ////鼠标跟随特效
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawLine(renderer, mouse_point_current.x, mouse_point_current.y, mouse_point_before.x, mouse_point_before.y);
        ////渲染后鼠标动作检测
        if (mouse_hold == MOUSE_BUTTON_LEFT && mouse_state == MOUSE_STATE_DRAG)
        {
            //表面的选择框，用于校准误差以及确保选择框的显示
            SDL_RenderDrawRect(renderer, &mouse_rect_select); // FIXME:这里好像还有bug，记得来修
        }
        ////记录鼠标位置
        mouse_point_before = mouse_point_current; //在鼠标特技渲染之后记录

        //渲染各种文本
        rDebugTextRect = {1, 1, 0, 0}; //要移动到原点
        fps = static_cast<int>(1.0f / elapsed);
        fps = fps - fps % 8 + 4; // fps稳定算法
        tps = 1000.0 / double(current_tick_count); //TODO: 新的FPS/TPS计算方法：使用系统时钟，每秒计算一次，使用GetSystemTime()获取时间
        tps = tps - tps % 10 + 10; // tps稳定算法
        RenderText(renderer, "FPS:" + to_string(fps), font, &rDebugTextRect);
        rDebugTextRect.y += rDebugTextRect.h + 2; // RenderText会把文字大小写回所给矩阵中来
        RenderText(renderer, "TPS:" + to_string(tps), font, &rDebugTextRect);
        rDebugTextRect.y += rDebugTextRect.h + 2;
        RenderText(renderer, "mouse_position:" + to_string(mouse_point_current.x) + ", " + to_string(mouse_point_current.y), font, &rDebugTextRect);
        rDebugTextRect.y += rDebugTextRect.h + 2;
        RenderText(renderer, "mouse_map_position:" + to_string(map_mouse_point_current.x) + ", " + to_string(map_mouse_point_current.y), font, &rDebugTextRect);
        rDebugTextRect.y += rDebugTextRect.h + 2;
        RenderText(renderer, "map_texture_rect:" + to_string(rMapTextureRect.x) + ", " + to_string(rMapTextureRect.y) + ", " + to_string(rMapTextureRect.w) + ", " + to_string(rMapTextureRect.h), font, &rDebugTextRect);
        rDebugTextRect.y += rDebugTextRect.h + 2;
        RenderText(renderer, "zoomlevel:" + to_string(zoomlevel), font, &rDebugTextRect);
        if (is_info_ship)
        {
            rDebugTextRect.y += rDebugTextRect.h + 2;
            RenderText(renderer, "ship pos:" + to_string(info_ship->pos.x) + ", " + to_string(info_ship->pos.y), font, &rDebugTextRect);
            rDebugTextRect.y += rDebugTextRect.h + 2;
            RenderText(renderer, "ship speed:" + to_string(info_ship->speed), font, &rDebugTextRect);
            rDebugTextRect.y += rDebugTextRect.h + 2;
            RenderText(renderer, "ship direction angle:" + to_string(info_ship->direction_angle), font, &rDebugTextRect);
            rDebugTextRect.y += rDebugTextRect.h + 2;
            RenderText(renderer, "ship target:" + to_string(info_ship->target_pos.x) + ", " + to_string(info_ship->target_pos.y), font, &rDebugTextRect);
        }

        //显示Render内容
        // SDL_Delay(1000 / 60); //限制帧率
        SDL_RenderPresent(renderer); //开了垂直同步不用限制

        //帧结束
        fps_tick_end = SDL_GetPerformanceCounter();
        elapsed = (fps_tick_end - fps_tick_start) / (float)SDL_GetPerformanceFrequency();
    }
    SDL_DestroyTexture(star_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
    return 0;
}